var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var router = require('./routes/api')
var app = express();

mongoose.connect('mongodb://localhost:27017/restuser',{ useNewUrlParser: true }).then(function(){
  console.log('connected to db');
});
mongoose.Promise = global.Promise;

app.use(express.static('public'));
app.use(bodyParser.json());
app.use('/api',router);

//Error handling middleware
app.use(function(err,req,res,next){
  res.send({error:err.message});
});

app.listen(4000,function(){
  console.log('connected...');
});

var mongoose = require('mongoose');
var schema = mongoose.Schema;

//create a Schema

var userSchema = new schema({
  name:{
    type:String,
    required:true
  },
  age:{
    type:Number
  },
  available:{
    type:Boolean,
    default:false
  },
});

var userModel = mongoose.model('userModel',userSchema);

module.exports = userModel;

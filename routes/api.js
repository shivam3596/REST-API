var express = require('express');
var userModel = require('../models/user');
var router = express.Router();

//get list of user from database
router.get('/users',function(req,res,next){
  userModel.find({name:req.query.name}).then(function(data){
    res.send(data);
  }).catch(next);
});

//add user in database
router.post('/users',function(req,res,next){
  userModel.create(req.body).then(function(user){
    res.send(user)
  }).catch(next);
});

//update user in database
router.put('/users/:id',function(req,res,next){
  userModel.findByIdAndUpdate({_id:req.params.id},req.body).then(function(){
    userModel.findOne({_id:req.params.id}).then(function(user){
      res.send(user);
    });
  }).catch(next);
});

//delete user from database
router.delete('/users/:id',function(req,res,next){
  userModel.findByIdAndRemove({_id:req.params.id}).then(function(user){
    res.send(user);
  }).catch(next);
});

module.exports = router;
